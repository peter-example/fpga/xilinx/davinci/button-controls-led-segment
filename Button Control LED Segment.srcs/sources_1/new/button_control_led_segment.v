module button_control_led_segment(
    input sys_clk ,
    input sys_rst_n,
    input [3:0] key,
    output reg [3:0] led,
    output [5:0] seg_sel,
    output [7:0] seg_led
);
reg [23:0] cnt;
reg [1:0] led_control;
reg toggle;

wire [19:0] data;
wire [ 5:0] point;
wire en;
wire sign;

reg [19:0] myValue;

always @ (posedge sys_clk or negedge sys_rst_n) begin
    if(!sys_rst_n)
        cnt <= 24'd0;
    else if(cnt < 24'd9_999_999) 
        cnt<= cnt + 1;
    else
        cnt<= 0;
end

always @(posedge sys_clk or negedge sys_rst_n) begin
    if(!sys_rst_n) begin
        led <= 4'b0000;
    end
    else if(key[0]== 0) 
        toggle<=1;
    else if(key[0]== 1 && toggle==1)
    begin
        toggle<=0; 
        led <= 4'b1000; 
        myValue<=myValue+20'd2;
    end
    else if(key[0]== 1 && cnt==24'd0)
    begin
        if (led==4'b1000)
            led<=4'b0000;
    end
end

seg_led u_seg_led(
    .clk (sys_clk),
    .rst_n (sys_rst_n),
    .seg_sel (seg_sel ), 
    .seg_led (seg_led ) ,
    .data (myValue), 
    .point (6'd2),
    .en (1),
    .sign (0)
);

endmodule

