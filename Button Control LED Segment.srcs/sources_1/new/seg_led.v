module seg_led(
	//input
	input clk,
	input rst_n,
	
	input  [19:0]	data,		//???????99_9999 20??????
	input  [5:0]	point,	//6????
	input  			en,		//???????
	input 			sign, 	//???
	
	//output
	output reg [5:0] seg_sel,  //???????
	output reg [7:0] seg_led	//??
);

//parameter define	???????????
localparam CLK_DIVIDE = 4'd10;		//??????10
localparam MAX_NUM	 = 13'd5000;	//5MHz??5000*200ns=1ms

//reg define
reg  [3:0]  	 clk_cnt ; 		// ???????
reg  			    dri_clk ; 		// ????????,5MHz
reg  [23:0]     num ; 			// 24?bcd????
reg  [12:0]     counter ; 		// ??????????
reg 		  	    flag ; 			// ????????counter???1ms?
reg  [2:0]      cnt_sel ; 		// ????????
reg  [3:0]      num_disp ; 	// ??????????
reg 				 dot_disp ;		// ???????????

//wire define
wire [3:0] data0 ; // ???
wire [3:0] data1 ; // ???
wire [3:0] data2 ; // ???
wire [3:0] data3 ; // ???
wire [3:0] data4 ; // ???
wire [3:0] data5 ; // ????

//**************************************
//**				 main code
//**************************************

//??????????????????
//assign ???????????**???
assign data0 = data % 4'd10; 				   // ???
assign data1 = data / 4'd10 % 4'd10 ;	   // ???
assign data2 = data / 7'd100 % 4'd10 ;    // ???
assign data3 = data / 10'd1000 % 4'd10 ;  // ???
assign data4 = data / 14'd10_000 % 4'd10; // ???
assign data5 = data / 17'd100_000; 		   // ????

//?????10?????????5MHz????????dri_clk
always @(posedge clk or negedge rst_n) begin
   if(!rst_n) begin
       clk_cnt <= 4'd0;
       dri_clk <= 1'b1;
   end
   else if(clk_cnt == CLK_DIVIDE/2 - 1'd1) begin
       clk_cnt <= 4'd0;
       dri_clk <= ~dri_clk;
   end
   else begin
       clk_cnt <= clk_cnt + 1'b1;
       dri_clk <= dri_clk;
   end
end

//?20?2?????8421?
//dri_clk ????????
//cnt_sel????????
always @ (posedge dri_clk or negedge rst_n)begin
	if(!rst_n)
		num <= 24'b0;
	else if(data5 || point[5])begin	//??6???
			num[23:20] <= data5;
			num[19:16] <= data4;
			num[15:12] <= data3;
			num[11:8]  <= data2;
			num[7:4]   <= data1;
			num[3:0]   <= data0;
		 //num[23:0] <= {data5,data4,data3,data2,data1,data0};
	end
	else if(data4 || point[4])begin	//??5???
		num[19:0] <= {data4,data3,data2,data1,data0};
		if(sign)
			num[23:20] <= 4'd11;   //6???'-'?
		else
			num[23:20] <= 4'd10;   //6????
	end
	else if(data3 || point[3])begin	//??4???
		num[15:0] <= {data3,data2,data1,data0};
		num[23:20] <= 4'd10;   //6????
		if(sign)
			num[19:16] <= 4'd11;   //5???'-'?
		else
			num[19:16] <= 4'd10;   //5????
	end
	else if(data2 || point[2])begin	//??3???
		num[11:0] <= {data2,data1,data0};
		num[23:20] <= 4'd10;   //6????
		num[19:16] <= 4'd10;   //5????
		if(sign)
			num[15:12] <= 4'd11;   //4???'-'?
		else
			num[15:12] <= 4'd10;   //4????
	end
	else if(data1 || point[1])begin	//??2???
		num[7:0] <= {data1,data0};
		num[23:20] <= 4'd10;   //6????
		num[19:16] <= 4'd10;   //5????
		num[15:12] <= 4'd10;   //4????
		if(sign)
			num[11:8] <= 4'd11;   //3???'-'?
		else
			num[11:8] <= 4'd10;   //3????
	end
	else if(data0 || point[0])begin	//??1???
		num[3:0] <= data0;
		num[23:20] <= 4'd10;   //6????
		num[19:16] <= 4'd10;   //5????
		num[15:12] <= 4'd10;   //4????
		num[11:8]  <= 4'd10;   //3????
		if(sign)
			num[7:4] <= 4'd11;   //2???'-'?
		else
			num[7:4] <= 4'd10;   //2????
	end	
end


//5MHz??1ms?????
always @ (posedge dri_clk or negedge rst_n)begin
	if(!rst_n)begin
		counter <= 13'b0;
		flag	  <= 1'b0;
 	end
	else if(counter < MAX_NUM - 1'b1)begin
		counter <= counter + 1'b1;
		flag	  <= 1'b0;
	end
	else begin
		counter <= 13'b0;
		flag	  <= 1'b1;
	end
end

//cnt_sel [2:0] ???????? ???
//??0-5 1ms ??????????
always @ (posedge dri_clk or negedge rst_n)begin 
	if(!rst_n)begin 
		cnt_sel = 3'b0;  //????
	end
	else if(flag)begin
		if(cnt_sel < 3'd5)
			cnt_sel = cnt_sel + 1'b1;
		else
			cnt_sel = 3'b0;
	end
	else
		cnt_sel = cnt_sel; 
end

//????????? ?????
always @ (posedge dri_clk or negedge rst_n)begin 
	if(!rst_n)begin 
		seg_sel <= 6'b111_111;		//6'd63
	end
	else begin
		if(en)begin   //en=1
			case(cnt_sel)
				3'd0: begin
					seg_sel  <= 6'b111_110;//??????1?
					num_disp <= num[3:0];
					dot_disp <= ~point[0];
				end
				3'd1: begin
					seg_sel  <= 6'b111_101;//??????2?
					num_disp <= num[7:4];
					dot_disp <= ~point[1];
				end				
				3'd2: begin
					seg_sel  <= 6'b111_011;//??????3?
					num_disp <= num[11:8];
					dot_disp <= ~point[2];
				end				
				3'd3: begin
					seg_sel  <= 6'b110_111;//??????4?
					num_disp <= num[15:12];
					dot_disp <= ~point[3];
				end				
				3'd4: begin
					seg_sel  <= 6'b101_111;//??????5?
					num_disp <= num[19:16];
					dot_disp <= ~point[4];
				end				
				3'd5: begin
					seg_sel  <= 6'b011_111;//??????6?
					num_disp <= num[23:20];
					dot_disp <= ~point[5];
				end	
				default: begin
					seg_sel  <= 6'b111_111;
				end				
			endcase
		end	
		else begin		//en=0
			seg_sel <= 6'b111111; 
			num_disp <= 4'b0;
			dot_disp <= 1'b1;		
		end
	end
end

//?????????
always @ (posedge dri_clk or negedge rst_n) begin
	if (!rst_n)
		seg_led <= 8'hc0;
	else begin
		case (num_disp)
			4'd0 : seg_led <= {dot_disp,7'b1000000}; //???? 0
			4'd1 : seg_led <= {dot_disp,7'b1111001}; //???? 1
			4'd2 : seg_led <= {dot_disp,7'b0100100}; //???? 2
			4'd3 : seg_led <= {dot_disp,7'b0110000}; //???? 3
			4'd4 : seg_led <= {dot_disp,7'b0011001}; //???? 4
			4'd5 : seg_led <= {dot_disp,7'b0010010}; //???? 5
			4'd6 : seg_led <= {dot_disp,7'b0000010}; //???? 6
			4'd7 : seg_led <= {dot_disp,7'b1111000}; //???? 7
			4'd8 : seg_led <= {dot_disp,7'b0000000}; //???? 8
			4'd9 : seg_led <= {dot_disp,7'b0010000}; //???? 9
			4'd10: seg_led <= 8'b11111111; //???????
			4'd11: seg_led <= 8'b10111111; //????(-)
			default:
				seg_led <= {dot_disp,7'b1000000};
		endcase
	end
end
endmodule
